
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect, get_object_or_404
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.generic import ListView, DetailView

from home.forms import Contact, UploadMembershipApplicationForm, FundProjectForm, DepositForm, RegistrationForm, \
    ContactForm, AuthForm, ProjectIDForm, WithdrawalForm, DepositForm, PasswordResetRequestForm, SetPasswordForm
from home.models import Contact, Project, UserProfile, Investment, Announcement, FAQ, TransactionLog, Deposit, ProjectUpdate, \
        CommissionPercentage, CommissionPayment, Developer, Dividend, Withdrawal, PledgeFAQ
from django.contrib.auth import views, authenticate, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect,  Http404, HttpResponse
from django.template import RequestContext
import datetime
from django.contrib.auth import login as auth_login
from django.utils import timezone
from django.contrib.messages import get_messages
from django.views.static import serve
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import os
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes, smart_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template import loader
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from crowdfund.settings import DEFAULT_FROM_EMAIL, MEDIA_ROOT, DEBUG, MEDIA_URL
from django.views.generic import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.db.models.query_utils import Q
from itertools import chain
from django.http import FileResponse
from axes.utils import reset

def home(request):
    try:
        featured_projects = Project.objects.order_by('-date_created').filter(status='Open for Funding').filter(unlisted=False)[0:3]
        if len(featured_projects)<3:
            new_projects = Project.objects.order_by('-date_created').filter(~Q(status='Open for Funding')).filter(unlisted=False)
            featured_projects = list(chain(featured_projects, new_projects))
    except:
        featured_projects = None

    return render(request, 'home/base.html',{'object_list':featured_projects})

@csrf_protect
def register(request):
    if request.user.is_authenticated():
        return redirect('account')

    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
            username=form.cleaned_data['username'].lower(),
            password=form.cleaned_data['password1'],
            email=form.cleaned_data['email'],
            first_name=form.cleaned_data['first_name'],
            last_name=form.cleaned_data['last_name']
            )
            user_profile = UserProfile.objects.create_user_profile(user=user)
            user = authenticate(username=form.cleaned_data['username'].lower(),password=form.cleaned_data['password1'])
            auth_login(request, user)
            return redirect('account')
    else:
        form = RegistrationForm()
    variables = RequestContext(request, {
    'form': form
    })

    return render_to_response(
    'home/register.html',
    variables,
    )

def register_success(request):
    return render_to_response(
    'home/success_pages/register_success.html',
    )

def login(request):
    if request.user.is_authenticated():
        return redirect('account')
    template_response = views.login(request, template_name='home/login.html',authentication_form=AuthForm)
    return template_response

def logout(request):
    template_response = views.logout(request)
    return redirect('index')

@login_required
def password_change_ajax(request):
    if request.is_ajax():
        template_response = views.password_change(request,template_name='home/password_change_modal.html')
        if type(template_response) != HttpResponseRedirect:
            template_response.context_data['password_change_form'] = template_response.context_data.pop('form')
        return template_response

@login_required
def password_change_done(request):
    password_change_form = PasswordChangeForm(request.user)
    messages.add_message(request,messages.SUCCESS, 'Password change successful.')
    return HttpResponse("success")

@csrf_protect
@login_required()
def account(request):
    membership_application_form = UploadMembershipApplicationForm()
    deposit_form = DepositForm()
    withdrawal_form = WithdrawalForm()
    password_change_form = PasswordChangeForm(request.user)
    return render(request, 'home/account.html',
                  {'membership_application_form': membership_application_form, 'deposit_form':deposit_form,
                   'withdrawal_form':withdrawal_form, 'password_change_form':password_change_form,'messages':get_messages(request)})

@method_decorator(csrf_protect, name='dispatch')
@method_decorator(login_required, name='dispatch')
class UploadMembershipApplicationFormView(generic.FormView):
    template_name = 'example_form.html'
    form_class = UploadMembershipApplicationForm

    def form_valid(self, form):
        if self.request.is_ajax():
            input_file = form.cleaned_data['application_form']
            membership_id = form.cleaned_data['membership_id']
            user = self.request.user

            if user.userprofile.application_form:
                if(os.path.isfile(user.userprofile.application_form.path)):
                    os.remove(user.userprofile.application_form.path)

            user.userprofile.application_form = input_file
            user.userprofile.coop_membership_id = membership_id
            user.userprofile.membership_application_date = datetime.datetime.now()
            user.userprofile.is_coop_member = 'In Process'
            user.userprofile.save()
            try:
                form.delete_temporary_files() #TODO:Fix temp_uploads not being emptied
            except Exception as e :
                print(e)
            messages.add_message(self.request,messages.SUCCESS, 'Membership application submitted')
            return HttpResponse("success")
        else:
            return redirect('account')
    def form_invalid(self, form):
        return render(self.request, 'home/account_verify_modal.html',{'membership_application_form':form})

@method_decorator(csrf_protect, name='dispatch')
@method_decorator(login_required, name='dispatch')
class UploadDepositFormView(generic.FormView):
    template_name = 'example_form.html'
    form_class = DepositForm

    def form_valid(self, form):
        print("Is Valid")
        if self.request.is_ajax():
            print ("Is Ajax")
            deposit_slip = form.cleaned_data['deposit_slip']
            user = self.request.user

            if Deposit.objects.filter(user=user).filter(status='PENDING').count() >= 5:
                messages.add_message(self.request,messages.ERROR, 'Too many pending deposit slip requests, please wait for confirmation.')
                return render(self.request, 'home/deposit_modal.html',{'deposit_form': form})

            deposit = Deposit.objects.create_deposit(user=self.request.user, deposit_slip_image=deposit_slip,
                                                     datetime=datetime.datetime.now())
            print(str(deposit.deposit_slip_image))
            deposit.save()
            try:
                form.delete_temporary_files()  # TODO:Fix temp_uploads not being emptied
            except Exception as e:
                print(e)
            messages.add_message(self.request,messages.SUCCESS, 'Deposit slip submitted')
            return HttpResponse("success")
            #current_url = resolve(self.request.path_info).url_name
            #return redirect(current_url)
        else:
            return redirect('account')

    def form_invalid(self, form):
        print("Is Invalid")
        return render(self.request, 'home/deposit_modal.html', {'deposit_form': form})



@method_decorator(csrf_protect, name='dispatch')
@method_decorator(login_required, name='dispatch')
class UploadWithdrawalRequestFormView(generic.FormView):
    template_name = 'example_form.html'
    form_class = WithdrawalForm

    def form_valid(self, form):
        if self.request.is_ajax():
            withdrawal_form_file = form.cleaned_data['withdrawal_form_file']
            user = self.request.user

            if Withdrawal.objects.filter(user=user).filter(status='PENDING').count() >= 5:
                messages.add_message(self.request,messages.ERROR, 'Too many pending withdrawal requests, please wait for confirmation')
                return render(self.request, 'home/withdrawal_modal.html',{'withdrawal_form': form})

            withdrawal = Withdrawal.objects.create_withdrawal(user=self.request.user, withdrawal_form_file=withdrawal_form_file,
                                                        datetime=datetime.datetime.now())
            withdrawal.save()
            try:
                form.delete_temporary_files()  # TODO:Fix temp_uploads not being emptied
            except Exception as e:
                print(e)
            messages.add_message(self.request,messages.SUCCESS, 'Withdrawal request submitted')
            return HttpResponse("success")
        else:
            return redirect('account')

    def form_invalid(self, form):
        print("Is Invalid")
        return render(self.request, 'home/withdrawal_modal.html', {'withdrawal_form': form})


@login_required()
@csrf_protect
def fund_project(request, pk):
    user = request.user
    # try:
    project = get_object_or_404(Project, pk=pk)
    if project.status != 'Open for Funding':
        messages.add_message(request, messages.ERROR, 'This project is not open for funding.')
        return redirect('project-detail',pk=pk, update=1)

    funding_date_is_end = False
    if project.date_end != None and project.date_end < datetime.date.today():
        funding_date_is_end = True
        messages.add_message(request, messages.ERROR, 'Funding period for this project has ended.')
        return redirect('project-detail', pk=pk, update=1)

    user_available_balance = user.userprofile.wallet_available_balance()
    minimum_investment = project.min_investment_in_cash
    min = minimum_investment

    if user_available_balance < minimum_investment and project.investment_raised() + user_available_balance < project.funding_goal:
        messages.add_message(request, messages.ERROR,
                             'You have insufficient balance. Minimum investment is Php {}.'.format(
                                 minimum_investment))

    #commission will be included in the total funding goal
    #commission = CommissionPercentage.objects.first()
    #commission_percentage = commission.commission_percentage_in_decimal
    #get commission and apply here

    # 10% of the total funds to raise
    maximum_percentage = project.max_investment_in_decimal
    max = maximum_percentage * project.funding_goal #TODO: check if clean from 1,000 down

    existing_pledges = Investment.objects.filter(user=user, project=project)
    amount_of_existing_pledges = 0
    for pledge in existing_pledges:
        amount_of_existing_pledges += pledge.amount

    # if remaining funds to raise is less than 50k, set min the remaining funds to raise
    remaining_funds_to_raise = project.funding_goal - project.investment_raised()
    if remaining_funds_to_raise < min:
        min = remaining_funds_to_raise
        max = remaining_funds_to_raise
    # if remaining funds to raise is lower than 10% of the total project, set max to remaining funds to raise
    if remaining_funds_to_raise < max:
        max = remaining_funds_to_raise
    max2 = maximum_percentage * project.funding_goal - amount_of_existing_pledges
    if max2 < max:
        max = max2
    if max <= 0:
        messages.add_message(request,messages.ERROR, 'Your total pledges have exceed the maximum pledge amount of {}% of the project funding goal'.format(int(100*maximum_percentage)))
        return redirect('project-detail',pk=pk, update=1)

    # commission will be included in the total funding goal
    # if 102% of available balance is less than the max
    # set 98% of the balance as the max
    #if user_available_balance * (1 + commission_percentage) < max:
    #    max = user_available_balance * (1 - commission_percentage)

    if user_available_balance < max:
        max = user_available_balance

    if max < min:
        min = max

    # commission will be included in the total funding goal
    #If kulang pera vs the min, bawal mag invest
    #if user_available_balance < min + commission_percentage * min:

        return redirect('project-detail', pk=pk, update=1)
    if user_available_balance < min:
        messages.add_message(request,messages.ERROR, 'Insufficient wallet amount')
        return redirect('project-detail',pk=pk, update=1)
    fund_form = FundProjectForm()

    REQUIRED_SHARE_CAPITAL = 5000

    if(project.requires_share_capital ):
        if(user.userprofile.get_total_share_capital() < REQUIRED_SHARE_CAPITAL and not user.userprofile.has_old_kapandesal_share_capital):
            return render(request,'home/share_capital_error.html')

    if(request.method == 'POST'):
        fund_form = FundProjectForm(data=request.POST)
        if fund_form.is_valid():
            amount = fund_form.cleaned_data['amount']
            password = fund_form.cleaned_data['password']
            date = timezone.now()
            if amount <= 0:
                messages.add_message(request, messages.ERROR, 'Invalid Investment Amount')
                return redirect('project-detail', pk=pk, update=1)

            if not request.user.check_password(password):
                fund_form.add_error("password", "Incorrect Password")
            else:
                if amount < minimum_investment and project.investment_raised() + amount < project.funding_goal:
                    messages.add_message(request, messages.ERROR, 'Minimum investment should be {} pesos.'.format(minimum_investment))
                    return redirect('project-detail', pk=pk, update=1)
                elif amount + project.investment_raised() > project.funding_goal:
                    messages.add_message(request, messages.ERROR, 'System Error')
                    return redirect('project-detail', pk=pk, update=1)

                # commission will be included in the total funding goal
                #if user.userprofile.wallet_available_balance() >= amount * (1 + commission_percentage) and amount + project.investment_raised() <= project.funding_goal:
                if user.userprofile.wallet_available_balance() >= amount and amount + project.investment_raised() <= project.funding_goal:
                    investment = Investment.objects.create_investment(user=user,amount=amount,datetime=date, project=project)
                    investment.save()
                    #commission_payment = CommissionPayment.objects.create_commission_payment(user=user,amount=commission_percentage*amount,
                    #                                                    investment=investment, datetime=date)
                    #commission_payment.save()
                    if project.investment_raised() >= project.funding_goal:
                        project.status = 'Fully Funded'
                        project.save()
                    TransactionLog.objects.create_transaction_log(user=user,amount=amount,date=date, description="Investment to project: " + str(pk))
                    messages.add_message(request,messages.SUCCESS, 'Pledge Successful')
                    return redirect('project-detail',pk=pk, update=1)
                #elif user.userprofile.wallet_available_balance() < amount * (1 + commission_percentage):
                elif user.userprofile.wallet_available_balance() < amount:
                    messages.add_message(request,messages.ERROR, 'You have insufficient wallet amount.')
                    return redirect('project-detail',pk=pk, update=1)
                else:
                    messages.add_message(request,messages.ERROR, 'Your investment amount is above the remaining funding goal.')
                    return redirect('project-detail',pk=pk, update=1)
    return render(request,'home/pledge.html', {'form':fund_form,'project_id':pk, 'min': min, 'max':max,
                                               'commission_percentage':0, 'project': project,
                                               'minimum_investment':minimum_investment, 'maximum_percentage': maximum_percentage,
                                               'funding_date_is_end':funding_date_is_end})
    # except Exception as e:
    #     pass
    #     print(e)
    #     return redirect('project-list')


def project_update(request, pk, project_update_id):
    try:
        project_update = ProjectUpdate.objects.get(id=project_update_id)
    except Exception as e:
        raise Http404("Project Update does not exist")
    return render(request, 'home/project_update.html', {'project_update': project_update})

@method_decorator(login_required, name='dispatch')
class PledgeList(ListView):
    template_name = 'home/pledge_list.html'
    def get_queryset(self):
        return self.request.user.userprofile.pledges()

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PledgeList, self).get_context_data(**kwargs)

        context['membership_application_form'] = UploadMembershipApplicationForm()
        context['deposit_form'] = DepositForm()
        context['withdrawal_form'] = WithdrawalForm()
        return context

@method_decorator(login_required, name='dispatch')
class DepositList(ListView):
    template_name = 'home/deposit_list.html'
    def get_queryset(self):
            return self.request.user.deposits.order_by('-datetime')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(DepositList, self).get_context_data(**kwargs)

        context['membership_application_form'] = UploadMembershipApplicationForm()
        context['deposit_form'] = DepositForm()
        context['withdrawal_form'] = WithdrawalForm()
        return context

@method_decorator(login_required, name='dispatch')
class WithdrawalList(ListView):
    template_name = 'home/withdrawal_list.html'
    def get_queryset(self):
            return self.request.user.withdrawals.order_by('-datetime')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(WithdrawalList, self).get_context_data(**kwargs)

        context['membership_application_form'] = UploadMembershipApplicationForm()
        context['deposit_form'] = DepositForm()
        context['withdrawal_form'] = WithdrawalForm()
        return context

@method_decorator(login_required, name='dispatch')
class InvestmentList(ListView):
    template_name = 'home/investment_list.html'
    def get_queryset(self):
        return self.request.user.investments.order_by('-datetime')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(InvestmentList, self).get_context_data(**kwargs)

        context['membership_application_form'] = UploadMembershipApplicationForm()
        context['deposit_form'] = DepositForm()
        context['withdrawal_form'] = WithdrawalForm()
        return context

@method_decorator(login_required, name='dispatch')
class DividendList(ListView):
    template_name = 'home/dividend_list.html'
    def get_queryset(self):
        return self.request.user.dividends.order_by('-datetime')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(DividendList, self).get_context_data(**kwargs)

        context['membership_application_form'] = UploadMembershipApplicationForm()
        context['deposit_form'] = DepositForm()
        context['withdrawal_form'] = WithdrawalForm()
        return context

class ProjectList(ListView):
    def get_queryset(self):
        return Project.objects.order_by('-date_created').filter(unlisted=False)

class ProjectDetail(DetailView):
    model = Project
    messages = {'error1':{'type':'danger','message':'Insufficient wallet amount or amount exceeds remaining funding goal'},
                'success1':{'type':'success','message':'Pledge Successful'}}

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ProjectDetail, self).get_context_data(**kwargs)

        context['messages'] = get_messages(self.request)
        paginator = Paginator(self.object.updates.order_by('-datetime'), 3)

        page = self.kwargs.get('update','1')
        try:
            updates = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            updates = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            updates = paginator.page(paginator.num_pages)
        context['updates'] = updates
        return context

class DeveloperDetail(DetailView):
    model = Developer

class AnnouncementList(ListView):
    model = Announcement

def faqs(request):
    return render(request, 'home/faq_list.html')

def about(request):
    return render(request,'home/about.html')

def contact(request):
    contact_form = None

    if request.method == 'POST':
        contact_form = ContactForm(data=request.POST)
        if contact_form.is_valid():
            try:
                contact = Contact(name=contact_form.cleaned_data['name'],
                        email=contact_form.cleaned_data['email'],
                        phone_number=contact_form.cleaned_data['phone_number'],
                        datetime=datetime.datetime.now(),
                        status=Contact.STATUS_CHOICES[0][0],
                        message=contact_form.cleaned_data['message'])
                contact.save()
                contact_form = ContactForm()
            except:
                pass
            return render_to_response('home/contact_success.html',)
        else:
            return render(request, 'home/contact.html',{'form':contact_form})
    else:
        contact_form = ContactForm()

    return render(request, 'home/contact.html',{'form':contact_form})

def media(request, filename, document_root=None, show_indexes=False):
    user = request.user
    print(filename + " ")
    to_serve = True
    protected_filename = "protected/"+filename
    if filename.split("/")[0] == 'membership_forms':
        if user.is_staff is False and UserProfile.objects.filter(user=user, application_form=protected_filename).count() == 0:
            to_serve = False
    elif  filename.split("/")[0] == 'deposit_slip_images':
        if user.is_staff is False and Deposit.objects.filter(user=user, deposit_slip_image=protected_filename).count() == 0:
            to_serve = False
    elif filename.split("/")[0] == 'deposit_slip_receipts_images':
        if user.is_staff is False and Deposit.objects.filter(user=user, receipt_image=protected_filename).count() == 0:
            to_serve = False
    elif filename.split("/")[0] == 'share_capital_certificates_images':
        if user.is_staff is False and Investment.objects.filter(user=user, share_capital_certificate=protected_filename).count() == 0:
            to_serve = False
    elif filename.split("/")[0] == 'dividend_receipts_images':
        if user.is_staff is False and Dividend.objects.filter(user=user, receipt_image=protected_filename).count() == 0:
            to_serve = False
    elif filename.split("/")[0] == 'withdrawal_form_images':
        if user.is_staff is False and Withdrawal.objects.filter(user=user, withdrawal_form_image=protected_filename).count() == 0:
            to_serve = False
    elif filename.split("/")[0] == 'withdrawal_receipts_images':
        if user.is_staff is False and Withdrawal.objects.filter(user=user, receipt_image=protected_filename).count() == 0:
            to_serve = False
    print(to_serve)
    #check if user owns it.
    if to_serve:
        if DEBUG is False:
            response = HttpResponse()
            url = MEDIA_URL + 'protected/' + filename
            #response['Content-Disposition'] = 'inline; filename=%s' % smart_str(filename)
            response['Content-Type'] = ''
            length = os.path.getsize(MEDIA_ROOT + "/protected/"+ filename)
            response['Content-Length'] = str(length)
            response['X-Accel-Redirect'] = url
            print("url: "+url)
            #print("res-cont: "+response['Content-Disposition'])
            print("res-accel: "+response['X-Accel-Redirect'])
            print("length:" +str(length))
            return response
        else:
            return serve(request, filename, document_root, show_indexes)
    else:
        return redirect('index')

def google_verification(request):
    return render(request, 'home/google2dc7e81a50daa76c.html')


def test_send_email(request):
    email = 'Test String Send to Matthew'
    send_mail('Test Send Email Successful', email, DEFAULT_FROM_EMAIL, ['matthew.go123@gmail.com',], fail_silently=False)

class ResetPasswordRequestView(FormView):
    template_name = 'home/account/forgot_password.html'  # code for template is given below the view's code
    success_url = '/login'
    form_class = PasswordResetRequestForm

    @staticmethod
    def validate_email_address(email):
        #This method here validates the if the input is an email address or not.
        #Its return type is boolean, True if the input is a email address or False if its not.
        try:
            validate_email(email)
            return True
        except ValidationError:
            return False


    def post(self, request, *args, **kwargs):
        #A normal post request which takes input from field "email_or_username" (in ResetPasswordRequestForm).

        form = self.form_class(request.POST)
        if form.is_valid():
            data = form.cleaned_data["email_or_username"].lower()
        if self.validate_email_address(data) is True:  # uses the method written above
            #If the input is an valid email address, then the following code will lookup for users associated with that email address.
            #If found then an email will be sent to the address, else an error message will be printed on the screen.

            associated_users = User.objects.filter(Q(email=data) | Q(username=data))
            if associated_users.exists():
                for user in associated_users:
                    c = {
                        'email': user.email,
                        'domain': request.META['HTTP_HOST'],
                        'site_name': 'Raise PH',
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'user': user,
                        'token': default_token_generator.make_token(user),
                        'protocol': 'http',
                    }
                    subject_template_name = 'registration/password_reset_subject.txt'
                    # copied from django/contrib/admin/templates/registration/password_reset_subject.txt to templates directory
                    email_template_name = 'registration/password_reset_email.html'
                    # copied from django/contrib/admin/templates/registration/password_reset_email.html to templates directory
                    subject = loader.render_to_string(subject_template_name, c)
                    # Email subject *must not* contain newlines
                    subject = ''.join(subject.splitlines())
                    email = loader.render_to_string(email_template_name, c)
                    send_mail(subject, email, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
                result = self.form_valid(form)
                messages.success(request,
                                 'An email has been sent to ' + data + ". Please check its inbox to continue resetting password.")
                return result
            result = self.form_invalid(form)
            messages.error(request, 'No user is associated with this email address')
            return result
        else:
            #If the input is an username, then the following code will lookup for users associated with that user.
            #If found then an email will be sent to the user's address, else an error message will be printed on the screen.

            associated_users = User.objects.filter(username=data)
            if associated_users.exists():
                for user in associated_users:
                    c = {
                        'email': user.email,
                        'domain': request.META['HTTP_HOST'],  # or your domain
                        'site_name': 'Raise PH',
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'user': user,
                        'token': default_token_generator.make_token(user),
                        'protocol': 'http',
                    }
                    subject_template_name = 'registration/password_reset_subject.txt'
                    email_template_name = 'registration/password_reset_email.html'
                    subject = loader.render_to_string(subject_template_name, c)
                    # Email subject *must not* contain newlines
                    subject = ''.join(subject.splitlines())
                    email = loader.render_to_string(email_template_name, c)
                    send_mail(subject, email, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
                result = self.form_valid(form)
                messages.success(request,
                                 'Email has been sent to ' + data + "'s email address. Please check its inbox to continue reseting password.")
                return result
            result = self.form_invalid(form)
            messages.error(request, 'This username does not exist in the system.')
            return result
        messages.error(request, 'Invalid Input')
        return self.form_invalid(form)


class PasswordResetConfirmView(FormView):
    template_name = "home/account/forgot_password.html"
    success_url = '/login/'
    form_class = SetPasswordForm

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
        View that checks the hash in a password reset link and presents a
        form for entering a new password.
        """
        UserModel = get_user_model()
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None


        if user is not None and default_token_generator.check_token(user, token):
            if form.is_valid():
                if (user.username != form.cleaned_data['username'].lower()):
                    messages.error(request, 'Invalid username')
                    return self.form_invalid(form)
                new_password= form.cleaned_data['new_password2']
                user.set_password(new_password)
                user.save()
                reset(username=user.username)
                messages.success(request, 'Password has been reset.')
                return self.form_valid(form)
            else:
                messages.error(request, 'Password reset has not been unsuccessful.')
                return self.form_invalid(form)
        else:
            messages.error(request,'The reset password link is no longer valid.')
            return self.form_invalid(form)