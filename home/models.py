import os

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator, MinValueValidator
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from decimal import Decimal
import uuid, datetime;
from datetime import date

from django.utils.html import format_html
from embed_video.fields import EmbedVideoField

phone_regex = RegexValidator(regex=r'^\+?1?\d{7,15}$',
                             message="Phone number must be entered in the format: '+9999999'. Up to 15 digits allowed.")

class UserProfileManager(models.Manager):
    def create_user_profile(self, user):
        user_profile = self.create(user=user)
        return user_profile

class UserProfile(models.Model):
    COOP_MEMBERSHIP_STATUS_CHOICES = (
        ('No', 'Not yet a member'),
        ('In Process', 'Application in process'),
        ('In Process - Old Member', 'Application in process - Old Member'),
        ('Waiting for First Deposit', 'Waiting for First Deposit'),
        ('Rejected', 'Rejected'),
        ('Yes', 'Confirmed'),
    )
    application_form_folder = 'membership_forms/'
    user = models.OneToOneField(User)
    contact_number = models.IntegerField(validators=[phone_regex], blank=True, null=True)
    application_form = models.FileField(upload_to='protected/'+application_form_folder, blank=True, null=True)
    #is_coop_member will be changed from 'Waiting for First Deposit' to 'Yes' if a MembershipFeePayment has been created for the user
    is_coop_member = models.CharField(choices=COOP_MEMBERSHIP_STATUS_CHOICES, max_length=50, default=COOP_MEMBERSHIP_STATUS_CHOICES[0][0])
    form_error_description = models.CharField(max_length=150, blank=True, null=True)
    membership_application_date = models.DateTimeField(blank=True, null=True)
    membership_approval_date = models.DateTimeField(blank=True, null=True)
    coop_membership_id = models.CharField(max_length=150, blank=True, null=True)
    has_old_kapandesal_share_capital = models.BooleanField(default=False)

    objects = UserProfileManager()

    def save(self, *args, **kwargs):

        # check if the deposit status was updated
        membership_status_changed = False
        if self.pk is not None:
            original = UserProfile.objects.get(pk=self.pk)
            if original.is_coop_member != self.is_coop_member:
                membership_status_changed = True

        # perform the actual db save
        super(UserProfile, self).save(*args, **kwargs)
        
        # send the e-mail
        message = 'Good day!<br><br>This is to inform you that the status of your Kapandesal membership is now \'<b>{}</b>\'.{}<br><br>For any concerns, please reply to this e-mail and we\'ll get back to you as soon as possible.<br><br>Thank you!<br>Raise PH Team<br><br><i>Note: This is an auto-generated e-mail. {}</i>'.format(dict(self.COOP_MEMBERSHIP_STATUS_CHOICES).get(self.is_coop_member),'<br/>Reason: <b>{}</b>'.format(self.form_error_description) if self.is_coop_member=='Rejected' else '', timezone.localtime(timezone.now()).strftime("%b %d, %Y - %I:%M:%S %p"))

        if membership_status_changed:
            send_mail(
                subject='Kapandesal Membership Status Update',
                message='',
                html_message=message,
                from_email='admin@raise.ph',
                recipient_list=[self.user.email],
                fail_silently=False,
            )

    def protected_application_form(self,):
        if self.application_form:
            url = reverse('file_serve', args=[self.application_form_folder+os.path.basename(self.application_form.file.name)])
            return format_html('<a href="'+url+'">File</a>')
            protected_application_form.short_description = 'Application Form'


    def paid_membership_fee(self):
        if hasattr(self.user, 'membership_fee_payment'):
            return True
        return False

    def wallet_total_balance(self):
        balance = 0
        try:
            for deposit in self.user.deposits.all(): #TODO: revise to only get confirmed deposits
                if(deposit.amount != None):
                    balance += deposit.amount
            for dividends in self.user.dividends.all():
                if (dividends.amount != None):
                    balance += dividends.amount
            for investment in self.user.investments.all():
                if investment.is_only_pledge == False and investment.amount != None:
                    balance -= investment.amount
            for withdrawal in self.user.withdrawals.all():
                if (withdrawal.status == 'APPROVED' and withdrawal.amount != None):
                    balance -= withdrawal.amount
            for commission_payment in self.user.commission_payments.all():
                if(commission_payment.investment.is_only_pledge() == False and commission_payment.amount != None):
                    balance -= commission_payment.amount
            for service_fee in self.user.service_fees.all():
                    balance -= service_fee.amount
            if hasattr(self.user, 'membership_fee_payment'):
                balance -= self.user.membership_fee_payment.amount
        except:
            pass
        return balance

    def wallet_available_balance(self):
        balance = 0
        for deposit in self.user.deposits.all():
            if (deposit.amount != None):
                balance += deposit.amount
        for dividends in self.user.dividends.all():
            if (dividends.amount != None):
                balance += dividends.amount
        for investment in self.user.investments.all():
            if investment.amount != None:
                balance -= investment.amount
        for withdrawal in self.user.withdrawals.all():
            if (withdrawal.status == 'PENDING' or withdrawal.status == 'APPROVED') and withdrawal.amount != None :
                balance -= withdrawal.amount
        for commission_payment in self.user.commission_payments.all():
            if (commission_payment.amount != None):
                balance -= commission_payment.amount
        for service_fee in self.user.service_fees.all():
            balance -= service_fee.amount
        if hasattr(self.user, 'membership_fee_payment'):
            balance -= self.user.membership_fee_payment.amount

        return balance

    def pledges(self):
        investments =  self.user.investments.all()
        pledges = []
        for i in investments:
            if i.is_only_pledge():
                pledges.append(i)
        return pledges

    def investments(self):
        investments =  self.user.investments.all()
        backed = []
        for i in investments:
            if not i.is_only_pledge():
                backed.append(i)
        return backed

    def pending_deposits(self):
        return self.user.deposits.filter(status='PENDING')

    def get_total_share_capital(self):
        total = 0
        investments =  self.user.investments.all()
        for i in investments:
            if (i.project.is_share_capital):
                total+= i.amount
        return total

class Developer(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=3000)
    location = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Project(models.Model):
    PROJECT_STATUS_CHOICES = (
        ('Pending for Approval', 'Pending for Approval'),
        ('Open for Funding', 'Open for Funding'),
        ('On Hold', 'On Hold'),
        ('Fully Funded', 'Fully Funded'),
        ('Constructed', 'Constructed'),
        ('Closed', 'Closed')
    )

    class Meta:
        ordering = ["-date_created"]

    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='project_images/')
    developer = models.ForeignKey(Developer,related_name='projects', on_delete=models.CASCADE)
    location = models.CharField(max_length=100)
    location_google_x = models.DecimalField(max_digits=20, decimal_places=10, validators=[MinValueValidator(Decimal('0'))])
    location_google_y  = models.DecimalField(max_digits=20, decimal_places=10, validators=[MinValueValidator(Decimal('0'))])
    funding_goal = models.DecimalField(max_digits=12, decimal_places=2,validators=[MinValueValidator(Decimal('0'))])
    estimated_profit_share = models.CharField(max_length=100)
    description = models.CharField(max_length=6000)
    date_created = models.DateField()
    date_end = models.DateField(blank=True, null=True)
    status = models.CharField(choices=PROJECT_STATUS_CHOICES, max_length=50, default=PROJECT_STATUS_CHOICES[0][0])
    fully_funded_date = models.DateTimeField(blank=True, null=True)
    unlisted = models.BooleanField(default=False)
    is_share_capital = models.BooleanField(default=True)
    requires_share_capital = models.BooleanField(default=False)

    min_investment_in_cash = models.IntegerField(blank=False)
    max_investment_in_decimal = models.DecimalField(blank=False, decimal_places=6, max_digits=8)

    def investment_raised(self):
        total = 0
        try:
            for investment in self.investments.all():
                total += investment.amount
        except:
            pass
        return total


    def investment_raised_ratio(self):
        ratio = 0
        try:
            ratio = (self.investment_raised() / self.funding_goal) * 100
        except:
            pass
        return ratio

    def __str__(self):
        return self.name

    @property
    def is_past_funding_deadline(self):
        if self.date_end == None:
            return False
        elif date.today() > self.date_end:
            return True
        return False

    def investor_count(self):
        count = 0
        investors = []
        try:
            for investment in self.investments.all():
                if investment.user not in investors:
                    investors.append(investment.user)
                    count+=1
        except:
            pass
        return count

class ProjectUpdate(models.Model):
    title = models.CharField(max_length=100, blank=False, null=False)
    project = models.ForeignKey(Project, related_name="updates", on_delete=models.CASCADE)
    content = models.CharField(max_length=1000, blank=False, null=False)
    video = EmbedVideoField(max_length=100, blank=True, null=True)
    datetime = models.DateTimeField(blank=False, null=False)
    #<object style="height: 390px; width: 640px"><param name="movie"
    # value="http://www.youtube.com/v/v1gTI4BOPUw?version=3">
    # <param name="allowFullScreen" value="true"><param name="allowScriptAccess" value="always">
    # <embed src="http://www.youtube.com/v/v1gTI4BOPUw?version=3"
    # type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="640" height="390"></object>

class ProjectUpdateImage(models.Model):
    project_update = models.ForeignKey(ProjectUpdate, related_name='images')
    image = models.ImageField()

class InvestmentManager(models.Manager):
    def create_investment(self, user, amount, datetime, project):
        reservation = self.create(user=user, amount=amount, datetime=datetime, project=project)
        return reservation

#For user pledges and backed up projects
class Investment(models.Model):

    share_capital_certificate_folder = 'share_capital_certificates_images/'
    user = models.ForeignKey(User,related_name='investments', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=12, decimal_places=2,validators=[MinValueValidator(Decimal('0'))])
    datetime = models.DateTimeField()
    project = models.ForeignKey(Project,related_name='investments', on_delete=models.CASCADE)
    share_capital_certificate = models.ImageField(upload_to='protected/'+share_capital_certificate_folder, blank=True, null=True)
    remarks = models.CharField(max_length=200, blank=True, null=True)

    def protected_share_capital_certificate(self,):
        if self.share_capital_certificate:
            url = reverse('file_serve', args=[self.share_capital_certificate_folder+os.path.basename(self.share_capital_certificate.file.name)])
            return format_html('<a href="'+url+'">File</a>')
            protected_share_capital_certificate.short_description = 'Share Capital Certificate'

    objects = InvestmentManager()
    def __str__(self):
        return '{} {} {}'.format(self.amount,self.user,self.project)

    def is_only_pledge(self):
        if(self.project.investment_raised_ratio() < 100):
            return True;
        else:
            return False;


class CommissionPaymentManager(models.Manager):
    def create_commission_payment(self, user, amount, investment, datetime):
        commission_payment = self.create(user=user, amount=amount, investment=investment, datetime=datetime)
        return commission_payment

class CommissionPayment(models.Model):
    user = models.ForeignKey(User, related_name='commission_payments', on_delete=models.CASCADE)
    investment = models.OneToOneField(Investment)
    amount = models.DecimalField(max_digits=12, decimal_places=2, validators=[MinValueValidator(Decimal('0'))],
                                 blank=True, null=True)
    datetime = models.DateTimeField()

    objects = CommissionPaymentManager()

class DepositManager(models.Manager):
    def create_deposit(self, user, deposit_slip_image, datetime):
        deposit = self.create(user=user, deposit_slip_image=deposit_slip_image, datetime=datetime)
        return deposit

class NullableCharField(models.CharField):
    description = "CharField that stores NULL but returns ''"
    __metaclass__ = models.SubfieldBase
    def to_python(self, value):
        if isinstance(value, models.CharField):
            return value
        return value or ''
    def get_prep_value(self, value):
        return value or None

#For users to fund their wallets
class Deposit(models.Model):
    STATUS_CHOICES = (
        ('PENDING', 'Pending'),
        ('DUPLICATE', 'Rejected - Duplicate Request'),
        ('REJECTED', 'Rejected - Other Reasons'),
        ('APPROVED', 'Approved')
    )
    deposit_slip_image_folder = 'deposit_slip_images/'
    receipt_image_folder = 'deposit_slip_receipts_images/'
    user = models.ForeignKey(User,related_name='deposits', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=12, decimal_places=2,validators=[MinValueValidator(Decimal('0'))], blank=True, null=True)
    deposit_slip_image = models.ImageField(upload_to='protected/'+deposit_slip_image_folder)
    datetime = models.DateTimeField()
    status = models.CharField(choices=STATUS_CHOICES, max_length=50, default=STATUS_CHOICES[0][0])
    comment = models.CharField(max_length=150, blank=True, null=True)
    deposit_reference_number = NullableCharField(max_length=100, default=None, blank=True, unique=True, null=True)  #TODO: unique true doesn't allow multiple blanks
    receipt_image = models.ImageField(upload_to='protected/'+receipt_image_folder, blank=True, null=True)

    def protected_deposit_slip_image(self,):
        if self.deposit_slip_image:
            url = reverse('file_serve', args=[self.deposit_slip_image_folder+os.path.basename(self.deposit_slip_image.file.name)])
            return format_html('<a href="'+url+'">File</a>')
            protected_deposit_slip_image.short_description = 'Deposit Slip Image'

    def deposit_slip_image_filename(self):
        return self.deposit_slip_image_folder+os.path.basename(self.deposit_slip_image.file.name)

    def protected_receipt_image(self,):
        if self.receipt_image:
            url = reverse('file_serve', args=[self.receipt_image_folder+os.path.basename(self.receipt_image.file.name)])
            return format_html('<a href="'+url+'">File</a>')
            protected_receipt_image.short_description = 'Receipt Image'

    objects = DepositManager()

    def __str__(self):
        return '{} {} Confirmed:{} '.format(self.user,self.amount,self.status)

    def save(self, *args, **kwargs):

        # check if the deposit status was updated
        deposit_status_changed = False
        if self.pk is not None:
            original = Deposit.objects.get(pk=self.pk)
            if original.status != self.status:
                deposit_status_changed = True
                
        # perform the actual db save
        super(Deposit, self).save(*args, **kwargs)
        
        # send the e-mail
        if deposit_status_changed:
            send_mail(
                subject='Deposit Status Changed',
                message='',
                html_message='Good day!<br><br>This is to inform you that the status of your deposit with reference number <b>{}</b> is now \'<b>{}</b>\'.{}<br><br>For any concerns, please reply to this e-mail and we\'ll get back to you as soon as possible.<br><br>Thank you!<br>Raise PH Team<br><br><i>Note: This is an auto-generated e-mail. {}</i>'.format(self.deposit_reference_number, dict(self.STATUS_CHOICES).get(self.status), '<br/>Reason: <b>{}</b>'.format(self.comment) if self.status=='REJECTED' else '', timezone.localtime(timezone.now()).strftime("%b %d, %Y - %I:%M:%S %p")),
                from_email='admin@raise.ph',
                recipient_list=[self.user.email],
                fail_silently=False,
            )
        
#For projects giving interest into user wallet
class Dividend(models.Model):
    receipt_image_folder='dividend_receipts_images/'
    user = models.ForeignKey(User,related_name='dividends', on_delete=models.CASCADE)
    investment = models.ForeignKey(Investment, related_name='dividends', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=12, decimal_places=2,validators=[MinValueValidator(Decimal('0'))])
    datetime = models.DateTimeField()
    receipt_image = models.ImageField(upload_to='protected/'+receipt_image_folder, blank=True, null=True)

    def protected_receipt_image(self,):
        if self.receipt_image:
            url = reverse('file_serve', args=[self.receipt_image_folder+os.path.basename(self.receipt_image.file.name)])
            return format_html('<a href="'+url+'">File</a>')
            protected_receipt_image.short_description = 'Receipt Image'

    def div_yield_in_percentage(self):
        return self.amount  * 100/ self.investment.amount

class WithdrawalManager(models.Manager):
    def create_withdrawal(self, user, withdrawal_form_file, datetime):
        withdrawal = self.create(user=user, withdrawal_form_file=withdrawal_form_file, datetime=datetime)
        return withdrawal

#For user withdrawing from wallet
class Withdrawal(models.Model):
    STATUS_CHOICES = (
        ('PENDING', 'Pending'),
        ('NOT_ENOUGH_BALANCE', 'Rejected - Not enough balance'),
        ('REJECTED', 'Rejected - Other Reasons'),
        ('APPROVED', 'Approved')
    )
    withdrawal_form_file_folder='withdrawal_form_images/'
    receipt_image_folder='withdrawal_receipts_images/'
    user = models.ForeignKey(User,related_name='withdrawals', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=12, decimal_places=2,validators=[MinValueValidator(Decimal('0'))], null=True, blank=True)
    datetime = models.DateTimeField()
    status = models.CharField(choices=STATUS_CHOICES, max_length=50, default=STATUS_CHOICES[0][0])
    withdrawal_form_file = models.ImageField(upload_to='protected/'+withdrawal_form_file_folder)
    withdrawal_reference_number = models.CharField(max_length=100, blank=True, unique=True, null=True) #TODO: unique true doesn't allow multiple blanks
    receipt_image = models.ImageField(upload_to='protected/'+receipt_image_folder, blank=True, null=True)

    def protected_withdrawal_form_file(self,):
        if self.withdrawal_form_file:
            url = reverse('file_serve', args=[self.withdrawal_form_file_folder+os.path.basename(self.withdrawal_form_file.file.name)])
            return format_html('<a href="'+url+'">File</a>')
            protected_withdrawal_form_file.short_description = 'Withdrawal Form File'

    def protected_receipt_image(self,):
        if self.receipt_image:
            url = reverse('file_serve', args=[self.receipt_image_folder+os.path.basename(self.receipt_image.file.name)])
            return format_html('<a href="'+url+'">File</a>')
            protected_receipt_image.short_description = 'Receipt Image'

    objects = WithdrawalManager()

    def __str__(self):
        return '{} {} Confirmed:{} '.format(self.user, self.amount, self.status)

class ServiceFee(models.Model):
    user = models.ForeignKey(User, related_name="service_fees", on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=12, decimal_places=2, validators=[MinValueValidator(Decimal('0'))])
    datetime = models.DateTimeField()
    reason = models.CharField(max_length=200)

class MembershipFeePayment(models.Model):
    user = models.OneToOneField(User, related_name='membership_fee_payment')
    amount = models.DecimalField(max_digits=12, decimal_places=2, validators=[MinValueValidator(Decimal('0'))])
    datetime = models.DateTimeField()

#For contact us / feedback page
class Contact(models.Model):

    STATUS_CHOICES = (
        ('PENDING', 'Pending'),
        ('RESOLVED', 'Resolved')
    )

    name = models.CharField(max_length=100)
    email = models.EmailField()
    phone_number = models.IntegerField(validators=[phone_regex])
    status = models.CharField(choices=STATUS_CHOICES, max_length=50, default=STATUS_CHOICES[0][0])
    datetime = models.DateTimeField()
    message = models.TextField()

    def __str__(self):
        return '{} - {} - {} - {}'.format(self.name,self.email,self.phone_number,self.message)

class TransactionLogManager(models.Manager):
    def create_transaction_log(self, user, amount, date, description):
        transactionCode = str(uuid.uuid4()).upper().replace("-","")
        #TODO: How to handle if transactionCode was duplicated?
        reservation = self.create(user=user, amount=amount, date=date, description=description, transactionCode=transactionCode)
        return reservation

class TransactionLog(models.Model):
    TRANSACTION_TYPE_CHOICES = (
        ('INVESTMENT', 'Investment'),
       ('DEPOSIT', 'Deposit'),
        ('DIVIDEND', 'Dividend'),
        ('WITHDRAWAL', 'Withdrawal'),
    )
    user = models.ForeignKey(User, related_name='transactions',on_delete=models.CASCADE)
    transactionCode = models.CharField(max_length=30, blank=False, unique=True)
    transactionType = models.CharField(max_length=30,blank=False, choices=TRANSACTION_TYPE_CHOICES)
    amount = models.DecimalField(max_digits=12, decimal_places=2,validators=[MinValueValidator(Decimal('0'))], blank=False)
    description = models.CharField(max_length=100, blank=True, null=True)
    date = models.DateField(blank=False)

    objects = TransactionLogManager();

class Announcement(models.Model):
    user = models.ForeignKey(User, related_name='announcements', on_delete=models.CASCADE)
    title = models.CharField(max_length=100, blank=False)
    content = models.CharField(max_length=1000)
    date = models.DateTimeField(blank=False, default=timezone.now)

class FAQ(models.Model):
    question = models.CharField(max_length=200, blank=False)
    answer = models.CharField(max_length=200, blank=False)

class PledgeFAQ(models.Model):
    question = models.CharField(max_length=200, blank=False)
    answer = models.CharField(max_length=200, blank=False)

class CommissionPercentage(models.Model):
    commission_percentage_in_decimal = models.DecimalField(blank=False, decimal_places=4, max_digits=6)

