from django import forms

def validate_doc_file_extension(value):
  import os
  ext = os.path.splitext(value.name)[1]
  valid_extensions = ['.pdf','.doc','.docx','.jpg','.jpeg','.png', '.PDF', '.DOC', '.DOCX', '.JPG', '.JPEG', '.PNG' ]
  if not ext in valid_extensions:
    raise forms.ValidationError(u'File not supported!')

def validate_image_file_extension(value):
  import os
  ext = os.path.splitext(value.name)[1]
  valid_extensions = ['.jpg','.jpeg','.png', '.JPG', '.JPEG', '.PNG']
  if not ext in valid_extensions:
    raise forms.ValidationError(u'File not supported!')
