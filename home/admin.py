from django.contrib import admin
from django.db import models

# Register your models here.
from home.forms import AnnouncementAdminForm, FAQAdminForm
from home.models import Contact, Developer, Investment, Project,ProjectUpdate,UserProfile, Deposit,Withdrawal, \
    Dividend, Announcement,FAQ, ProjectUpdateImage, MembershipFeePayment, \
    CommissionPercentage, CommissionPayment, ServiceFee

from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
import datetime

class ContactAdmin(admin.ModelAdmin):
    list_display = ('name','email','phone_number','message','datetime','status')
    search_fields = ('message','name')
    list_filter = ('datetime', 'status')
    ordering = ('datetime', 'name')

class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ('user', 'title', 'content', 'date')
    search_fields = ('title',)
    list_filter = ('date',)
    ordering = ('date', 'user')
    form = AnnouncementAdminForm

class FAQAdmin(admin.ModelAdmin):
    list_display = ('question', 'answer')
    ordering = ('question', 'answer')
    form = FAQAdminForm

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False

class UserInline(admin.StackedInline):
    model = User
    can_delete = False

class UserAdmin(UserAdmin):
    search_fields = ('username', 'first_name', 'last_name', 'email')
    inlines = (UserProfileInline,)

class UserProfileAdmin(admin.ModelAdmin):

    readonly_fields = ('protected_application_form',)
    list_display = ('user', 'first_name', 'last_name', 'coop_membership_id', 'email', 'protected_application_form','is_coop_member',
                    'paid_membership_fee', 'membership_application_date', 'membership_approval_date',
                    'wallet_total_balance', 'wallet_available_balance','contact_number')
    search_fields = ('user__username', 'user__first_name', 'user__last_name', 'user__email')
    list_filter = ('is_coop_member',)
    list_editable = ('coop_membership_id',)
    ordering = ('user', 'is_coop_member', 'membership_application_date', 'membership_approval_date',
                'user__first_name', 'user__last_name', 'user__email', 'coop_membership_id')

    def first_name(self, obj):
        return '%s' % (obj.user.first_name)
    first_name.admin_order_field = 'user__first_name'
    def last_name(self, obj):
        return '%s' % (obj.user.last_name)
    last_name.admin_order_field = 'user__last_name'
    def email(self, obj):
        return '%s' % (obj.user.email)
    email.admin_order_field = 'user__email'

class DepositAdmin(admin.ModelAdmin):
    readonly_fields = ( 'first_name', 'last_name', 'email','protected_deposit_slip_image','protected_receipt_image')
    list_display = ('user', 'last_name', 'first_name', 'email','amount', 'datetime', 'protected_deposit_slip_image', 'status', 'deposit_reference_number')
    search_fields = ('user__username', 'deposit_reference_number', 'deposit_reference_number','user__first_name', 'user__last_name', 'user__email')
    list_filter = ( 'datetime', 'status')
    ordering = ('user', 'amount', 'datetime', 'status', 'deposit_reference_number','user__first_name', 'user__last_name', 'user__email')

    def first_name(self, obj):
        return '%s' % (obj.user.first_name)
    first_name.admin_order_field = 'user__first_name'

    def last_name(self, obj):
        return '%s' % (obj.user.last_name)
    last_name.admin_order_field = 'user__last_name'

    def email(self, obj):
        return '%s' % (obj.user.email)

    email.admin_order_field = 'user__email'


class InvestmentAdmin(admin.ModelAdmin):
    readonly_fields = ('first_name', 'last_name', 'email','protected_share_capital_certificate')
    list_display = ('user', 'last_name', 'first_name', 'email','amount', 'datetime', 'project_name', 'is_only_pledge', 'remarks')
    search_fields = ('user__username', 'project__name','user__first_name', 'user__last_name', 'user__email', 'remarks')
    list_filter = ('project__name','remarks',)
    ordering = ('user', 'amount', 'datetime' , 'project__name','user__first_name', 'user__last_name', 'user__email', 'remarks')

    def project_name(self, obj):
        return '%s' % (obj.project.name)
    project_name.admin_order_field = 'project__name'
    def first_name(self, obj):
        return '%s' % (obj.user.first_name)
    first_name.admin_order_field = 'user__first_name'

    def last_name(self, obj):
        return '%s' % (obj.user.last_name)
    last_name.admin_order_field = 'user__last_name'

    def email(self, obj):
        return '%s' % (obj.user.email)
    email.admin_order_field = 'user__email'


    def save_model(self, request, obj, form, change):
        obj.save()
        if obj.project.investment_raised() >= obj.project.funding_goal:
            obj.project.status = 'Fully Funded'
            obj.project.fully_funded_date = datetime.datetime.now()
            obj.project.save()
        # elif obj.project.investment_raised() <= obj.project.funding_goal: # Doesn't consider on-hold projects etc
        #     obj.project.status = 'Open for Funding'
        #     obj.project.fully_funded_date = None
        #     obj.project.save()

    def delete_model(self, request, obj):
        obj.delete()
        if obj.project.investment_raised() <= obj.project.funding_goal:
            obj.project.status = 'On Hold'
            obj.project.fully_funded_date = None
            obj.project.save()

class CommissionPaymentAdmin(admin.ModelAdmin):
    list_display = ('user', 'amount', 'investment_amount', 'datetime',)
    search_fields = ('user__username','datetime',)
    list_filter = ('datetime',)
    ordering = ('user', 'amount', 'datetime',)

    def investment_amount(self, obj):
        return '%s' % (obj.investment.amount)

class WithdrawalAdmin(admin.ModelAdmin):
    readonly_fields = ('first_name', 'last_name', 'email','protected_withdrawal_form_file','protected_receipt_image')
    list_display = ('user', 'first_name', 'last_name', 'email', 'protected_withdrawal_form_file', 'amount', 'datetime', 'status')
    search_fields = ('user__username', 'status', 'user__first_name', 'user__last_name', 'user__email')
    list_filter = ('status',)
    ordering = ('user', 'amount', 'datetime', 'status','user__first_name', 'user__last_name', 'user__email')
    def first_name(self, obj):
        return '%s' % (obj.user.first_name)
    first_name.admin_order_field = 'user__first_name'

    def last_name(self, obj):
        return '%s' % (obj.user.last_name)
    last_name.admin_order_field = 'user__last_name'

    def email(self, obj):
        return '%s' % (obj.user.email)
    email.admin_order_field = 'user__email'

class DividendAdmin(admin.ModelAdmin):
    readonly_fields = ('protected_receipt_image',)
    list_display = ('user', 'first_name', 'last_name', 'email', 'amount', 'datetime', 'project_name')
    search_fields = ('user__username', 'investment__project__name', 'user__first_name', 'user__last_name', 'user__email')
    list_filter = ('investment__project__name',)
    ordering = ('user', 'amount', 'datetime' , 'investment__project__name', 'user__first_name', 'user__last_name', 'user__email')

    def project_name(self, obj):
        return '%s' % (obj.investment.project.name)
    project_name.admin_order_field = 'investment__project__name'

    def first_name(self, obj):
        return '%s' % (obj.user.first_name)
    first_name.admin_order_field = 'user__first_name'

    def last_name(self, obj):
        return '%s' % (obj.user.last_name)
    last_name.admin_order_field = 'user__last_name'

    def email(self, obj):
        return '%s' % (obj.user.email)
    email.admin_order_field = 'user__email'

class MembershipFeePaymentAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'last_name', 'amount', 'datetime',)
    search_fields = ('user__username','user__first_name','user__last_name')
    ordering = ('user', 'amount', 'datetime',)

    def save_model(self, request, obj, form, change):
        obj.save()
        if obj.user.userprofile.is_coop_member == 'Waiting for First Deposit':
            obj.user.userprofile.is_coop_member = 'Yes'
            obj.user.userprofile.save()

    def first_name(self, obj):
        return '%s' % (obj.user.first_name)
    first_name.admin_order_field = 'user__first_name'

    def last_name(self, obj):
        return '%s' % (obj.user.last_name)
    last_name.admin_order_field = 'user__last_name'

    def delete_model(self, request, obj):
        obj.user.userprofile.paid_membership_fee = False
        obj.user.userprofile.save()
        obj.delete()

class ServiceFeeAdmin(admin.ModelAdmin):
    list_display = ('user', 'amount', 'datetime', 'reason')
    search_fields = ('user__username',)
    ordering = ('user', 'amount', 'datetime',)

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'developer', 'investment_raised', 'funding_goal', 'investment_raised_ratio', 'date_created', 'date_end', 'status', 'fully_funded_date','min_investment_in_cash','max_investment_in_decimal')
    search_fields = ('name', 'developer', 'status')
    list_filter = ('developer', 'status', 'date_created', 'date_end',)
    ordering = ('name', 'developer',  'funding_goal', 'date_created', 'date_end', 'status')

    def save_model(self, request, obj, form, change):
        obj.save()
        if obj.investment_raised() >= obj.funding_goal:
            obj.status = 'Fully Funded'
            obj.fully_funded_date = datetime.datetime.now()
            obj.save()

class ProjectUpdateImageInline(admin.TabularInline):
    model = ProjectUpdateImage
    extra = 3

class ProjectUpdateAdmin(admin.ModelAdmin):
    list_display = ('title', 'project_name','content', 'video', 'datetime')
    search_fields = ('title', 'project__name')
    list_filter = ('title', 'project__name', 'datetime')
    ordering = ('title', 'project__name', 'datetime')

    inlines = [ProjectUpdateImageInline, ]

    def project_name(self, obj):
        return '%s' % (obj.project.name)

    project_name.admin_order_field = 'project__name'

class CommissionPercentageAdmin(admin.ModelAdmin):
    list_display = ('commission_percentage_in_decimal',)

    def has_add_permission(self, request):
        return False if self.model.objects.count() > 0 else True



admin.site.register(Contact,ContactAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectUpdate, ProjectUpdateAdmin)
admin.site.register(Investment, InvestmentAdmin)
admin.site.register(Deposit, DepositAdmin)
admin.site.register(Withdrawal, WithdrawalAdmin)
admin.site.register(Dividend, DividendAdmin)
admin.site.register(MembershipFeePayment, MembershipFeePaymentAdmin)
admin.site.register(Developer)
admin.site.register(Announcement, AnnouncementAdmin)
#admin.site.register(FAQ, FAQAdmin)
admin.site.register(CommissionPercentage, CommissionPercentageAdmin)
admin.site.register(CommissionPayment, CommissionPaymentAdmin)
admin.site.register(ServiceFee, ServiceFeeAdmin)

admin.site.unregister(User)
admin.site.register(User,UserAdmin)
admin.site.register(UserProfile, UserProfileAdmin)

def download_csv(modeladmin, request, queryset):
    import csv
    from django.http import HttpResponse
    from io import StringIO

    f = StringIO()
    writer = csv.writer(f)
    # writer.writerow(["code", "country", "ip", "url", "count"])
    #
    # for s in queryset:
    #     writer.writerow([s.code, s.country, s.ip, s.url, s.count])

    headers = []
    model = queryset.model

    appends = ['UserProfile','Investment','Deposit','CommissionPayment','Dividend','Withdrawal','ServiceFee','MembershipFeePayment','TransactionLog','Announcement']
    if(model.__name__ in appends):
        for field in model._meta.fields:
            headers.append(field.name)
        headers.insert(2,'first_name')
        headers.insert(3,'last_name')
        if (model.__name__ == 'UserProfile'):
            headers.insert(4,'Wallet total balance')
            headers.insert(5,'Wallet available balance')
        if (model.__name__ == 'Investment' or model.__name__ == 'Deposit' or model.__name__ == 'UserProfile'):
            headers.insert(4,'email')
        writer.writerow(headers)
        for obj in queryset:
            row = []
            for field in headers:
                if field == 'first_name' or field == 'last_name':
                    val = getattr(User.objects.get(username=getattr(obj, 'user')),field)
                elif field == 'Wallet total balance':
                    val = UserProfile.objects.get(user=getattr(obj, 'user')).wallet_total_balance()
                elif field == 'Wallet available balance':
                    val = UserProfile.objects.get(user=getattr(obj, 'user')).wallet_available_balance()
                elif field == 'email':
                    val = getattr(User.objects.get(username=getattr(obj, 'user')),field)
                else:
                    val = getattr(obj, field)
                    # if callable(val):
                    # 	val = val()
                    # if type(val) == unicode:
                    # 	val = val.encode("utf-8")
                row.append(val)
            writer.writerow(row)
    else:
        for field in model._meta.fields:
            headers.append(field.name)
        writer.writerow(headers)
        for obj in queryset:
            row = []
            for field in headers:
                val = getattr(obj, field)
                # if callable(val):
                # 	val = val()
                # if type(val) == unicode:
                # 	val = val.encode("utf-8")
                row.append(val)
            writer.writerow(row)

    f.seek(0)
    response = HttpResponse(f, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename='+model.__name__+'-report.csv'
    return response

admin.site.add_action(download_csv, 'Export and download csv')