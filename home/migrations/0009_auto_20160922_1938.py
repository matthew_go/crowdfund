# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-22 11:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0008_project_unlisted'),
    ]

    operations = [
        migrations.DeleteModel(
            name='MaximumInvestment',
        ),
        migrations.DeleteModel(
            name='MinimumInvestment',
        ),
        migrations.AddField(
            model_name='project',
            name='max_investment_in_decimal',
            field=models.DecimalField(decimal_places=4, default=0.1, max_digits=6),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='project',
            name='min_investment_in_cash',
            field=models.IntegerField(default=50000),
            preserve_default=False,
        ),
    ]
