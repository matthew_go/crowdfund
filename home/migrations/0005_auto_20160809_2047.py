# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-09 12:47
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_auto_20160809_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='announcement',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 9, 20, 47, 16, 924478)),
        ),
    ]
