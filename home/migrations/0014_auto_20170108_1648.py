# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-01-08 08:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0013_auto_20161011_1102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='status',
            field=models.CharField(choices=[('Pending for Approval', 'Pending for Approval'), ('Open for Funding', 'Open for Funding'), ('On Hold', 'On Hold'), ('Fully Funded', 'Fully Funded'), ('Constructed', 'Constructed'), ('Closed', 'Closed')], default='Pending for Approval', max_length=50),
        ),
    ]
