from django.conf.urls import url, include
from django.conf.urls.static import static

from crowdfund import settings
from home import views
from axes.decorators import watch_login

urlpatterns = [
	url(r'^$', views.home, name='index'),

	#project
	url(r'^projects/$',views.ProjectList.as_view(),name='project-list'),
	url(r'^projects/(?P<pk>[0-9]+)/',  include([
		url(r'^(?P<update>[0-9]+)/$', views.ProjectDetail.as_view(), name='project-detail'),
		url(r'^pledge/$', views.fund_project, name='pledge'),
	])),
	url(r'^developer/(?P<pk>[0-9]+)/$',views.DeveloperDetail.as_view(),name='developer-detail'),
	# url(r'^projects/pledge$',views.fund_project_ajax,name='pledge-ajax'),

	#register/login
	url(r'^register/$', views.register, name='register'),
	url(r'^register_success/$', views.register_success, name='register_success'),
	url(r'^login/$', views.login, name='login'),
	url(r'^logout/$', views.logout, name='logout'),

	#account
	url(r'^account/$', views.account, name='account'),
	url(r'^account/password_change/ajax/$', views.password_change_ajax, name='password_change_ajax'),
	url(r'^account/password_change/done/$', views.password_change_done, name='password_change_done'),
	url(r'^upload/', include('django_file_form.urls')),
	url(r'^account/application_file_upload/ajax/$', views.UploadMembershipApplicationFormView.as_view(), name='application_file_upload'),
	url(r'^account/withdrawal_file_upload/ajax/$', views.UploadWithdrawalRequestFormView.as_view(),
					  name='withdrawal_file_upload'),
	url(r'^account/deposit_file_upload/ajax/$', views.UploadDepositFormView.as_view(),
		  name='deposit_file_upload'),
	url(r'^account/pledges/$', views.PledgeList.as_view(), name='account_pledges'),
	url(r'^account/investments$',views.InvestmentList.as_view(), name='account_investments'),
	url(r'^account/deposits/$', views.DepositList.as_view(), name='account_deposits'),
	url(r'^account/withdrawals/$', views.WithdrawalList.as_view(), name='account_withdrawals'),
	url(r'^account/dividends/$', views.DividendList.as_view(), name='account_dividends'),

	#others
	url(r'^announcements/$', views.AnnouncementList.as_view(), name='announcement_list'),
	url(r'^faqs/$', views.faqs, name='faq_list'),
	url(r'^about/$', views.about, name='about'),
	url(r'^contact/$', views.contact, name='contact'),
	url(r'^protected_media/(?P<filename>.*)$', views.media,{'document_root': settings.MEDIA_ROOT},name='file_serve'),

	url(r'^test_send_email/$', views.test_send_email, name='test_send_email'),
	  # url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', PasswordResetConfirmView.as_view(),name='reset_password_confirm'),
	  # PS: url above is going to used for next section of implementation.

	  url(r'^google2dc7e81a50daa76c.html/$', views.google_verification, name='google_verification'),

	url(r'^account/reset_password/$', views.ResetPasswordRequestView.as_view(), name="reset_password"),
	url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
		  views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
