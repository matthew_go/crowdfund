from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from decimal import Decimal

from django.template.defaultfilters import filesizeformat
from django_file_form.forms import FileFormMixin, UploadedFileField

from django.contrib.auth.forms import AuthenticationForm

from crowdfund import settings
from home.models import Contact, Announcement, FAQ
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
import re

from home.validators import validate_doc_file_extension, validate_image_file_extension

pattern = re.compile('^\w+$');

class RegistrationForm(forms.Form):

    MIN_USERNAME_LENGTH = 4
    MIN_PASSWORD_LENGTH = 8
    MIN_NAME_LENGTH = 2

    username = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Username *','required':'','data-validation-required-message':'Please enter a username.'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Email Address *','required':'','data-validation-required-message':'Please enter email.'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password *','required':'','data-validation-required-message':'Please enter a password.'
        }))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Repeat Password *','required':'','data-validation-required-message':'Please enter a password.'
        }))

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'First Name *', 'required': '',
               'data-validation-required-message': 'Please enter a first name.'}))
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Last Name *', 'required': '',
               'data-validation-required-message': 'Please enter a last name.'}))
    def clean_username(self):
        username = self.cleaned_data['username']
        if len(username) < self.MIN_USERNAME_LENGTH:
            raise forms.ValidationError("Username must be at least %d characters long." % self.MIN_USERNAME_LENGTH)
        if not pattern.match(username):
            raise forms.ValidationError("Username must only be composed of letters, numbers, & underscores.")
        try:
            user = User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_("The username already exists. Please try another one."))

    def clean_email(self):
        try:
            email = User.objects.get(email__iexact=self.cleaned_data['email'])
        except User.DoesNotExist:
            return self.cleaned_data['email']
        raise forms.ValidationError(_("The email already exists. Please try another one."))

    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']
        if len(first_name) < self.MIN_NAME_LENGTH:
            raise forms.ValidationError("First Name must be at least %d characters long." % self.MIN_NAME_LENGTH)
        return self.cleaned_data['first_name']

    def clean_last_name(self):
        last_name = self.cleaned_data['last_name']
        if len(last_name) < self.MIN_NAME_LENGTH:
            raise forms.ValidationError("Last Name must be at least %d characters long." % self.MIN_NAME_LENGTH)
        return self.cleaned_data['last_name']

    def clean(self):
        password = self.cleaned_data['password1']
        if len(password) < self.MIN_PASSWORD_LENGTH:
            raise forms.ValidationError("Password must be at least %d characters long." % self.MIN_PASSWORD_LENGTH)

        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields did not match."))
        return self.cleaned_data

class ContactForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(ContactForm,self).__init__(*args,**kwargs)

        self.fields['name'].widget = forms.TextInput(attrs={'class':'form-control','placeholder':'Your Name *','required':'','data-validation-required-message':'Please enter a name.'})
        self.fields['email'].widget = forms.EmailInput(attrs={'class':'form-control','placeholder':'Your Email *','required':'','data-validation-required-message':'Please enter a email address.'})
        self.fields['phone_number'].widget = forms.NumberInput(attrs={'class':'form-control','placeholder':'Your Phone *','required':'','data-validation-required-message':'Please enter a number.'})
        self.fields['message'].widget = forms.Textarea(attrs={'class':'form-control','placeholder':'Your Message *','required':'','data-validation-required-message':'Please enter a message.'})
    class Meta:
        model = Contact
        fields = ['name','email','phone_number','message']

def validate_applicationform_extension(value):
    import os
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf','.jpg','.png','.jpeg']
    if not ext.lower() in valid_extensions:
        raise ValidationError('File Extensions allowed: '+ u', '.join(map(str,valid_extensions)))

def validate_image_extension(value):
    import os
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.jpg','.png','.jpeg']
    if not ext.lower() in valid_extensions:
        raise ValidationError('File Extensions allowed: '+ u', '.join(map(str,valid_extensions)))

class UploadMembershipApplicationForm(FileFormMixin,forms.Form):
    application_form = UploadedFileField(required=False, label='',validators=[validate_applicationform_extension])
    membership_id = forms.CharField(max_length=100, required=False, label='If existing Kapandesal member, input Membership ID')

    def clean(self):
        if 'application_form' in self.cleaned_data and 'membership_id' in self.cleaned_data:
            if self.cleaned_data['application_form'] is None and \
                (self.cleaned_data['membership_id'] is None or self.cleaned_data['membership_id'].strip() == ''):
                raise forms.ValidationError(_("Application Form or Membership ID is required."))
            elif self.cleaned_data['application_form'] is not None and \
                (self.cleaned_data['membership_id'] is not None and self.cleaned_data['membership_id'].strip() != ''):
                raise forms.ValidationError(_("Application Form and Membership cannot have values at the same time."))

        return self.cleaned_data

    def clean_application_form(self):

        if 'application_form' in self.cleaned_data:
            application_form = self.cleaned_data['application_form']
            if application_form is not None:
                if application_form._size > settings.MAX_UPLOAD_SIZE:
                    raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') % (filesizeformat(settings.MAX_UPLOAD_SIZE), filesizeformat(application_form._size)))
        return application_form


class DepositForm(FileFormMixin,forms.Form):
    deposit_slip = UploadedFileField(validators=[validate_image_extension])

    def clean_deposit_slip(self):
        deposit_slip = self.cleaned_data['deposit_slip']
        if deposit_slip._size > settings.MAX_UPLOAD_SIZE:
            raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') % (filesizeformat(settings.MAX_UPLOAD_SIZE), filesizeformat(deposit_slip._size)))
        return deposit_slip

class WithdrawalForm(FileFormMixin,forms.Form):
    withdrawal_form_file = UploadedFileField(validators=[validate_image_extension])

    def clean_withdrawal_form_file(self):
        withdrawal_form_file = self.cleaned_data['withdrawal_form_file']
        if withdrawal_form_file._size > settings.MAX_UPLOAD_SIZE:
            raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') % (filesizeformat(settings.MAX_UPLOAD_SIZE), filesizeformat(withdrawal_form_file._size)))
        return withdrawal_form_file

class FundProjectForm(forms.Form):
    amount = forms.DecimalField(validators=[MinValueValidator(Decimal('0'))])
    slider = forms.DecimalField(validators=[MinValueValidator(Decimal('0'))])
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Password *', 'required': '',
               'data-validation-required-message': 'Please enter a password.'}))

class ProjectIDForm(forms.Form):
    project_id = forms.IntegerField(widget=forms.HiddenInput())

class AuthForm(AuthenticationForm):
    def __init__(self,*args,**kwargs):
        super(AuthForm,self).__init__(*args,**kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={'class':'form-control','placeholder':'Username *','required':'','data-validation-required-message':'Please enter a username.'})
        self.fields['password'].widget = forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password *','required':'','data-validation-required-message':'Please enter a password.'})


    def clean(self):
        username = self.cleaned_data.get('username').lower()
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

class AnnouncementAdminForm(forms.ModelForm):
    class Meta:
        model = Announcement
        fields = '__all__'
        widgets = {
            'content': forms.Textarea
        }

class FAQAdminForm(forms.ModelForm):
    class Meta:
        model = FAQ
        fields = '__all__'
        widgets = {
            'answer': forms.Textarea,
            'question': forms.Textarea
        }

class PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Email or Username *','required':'',
                                                                      'data-validation-required-message':'Please enter an username or email.'}))

class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        }
    username = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Username *', 'required': '',
               'data-validation-required-message': 'Please enter a username.'}))
    new_password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Password *', 'required': '',
               'data-validation-required-message': 'Please enter a password.'
               }))
    new_password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Repeat Password *', 'required': '',
               'data-validation-required-message': 'Please enter a password.'
               }))

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                    )
        return password2