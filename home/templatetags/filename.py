import os

from django import template

register = template.Library()

@register.filter
def protected_filename_url(value):
    return value.replace('/media/protected/','',1)