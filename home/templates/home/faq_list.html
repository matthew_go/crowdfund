{% extends 'home/base.html' %}

{% block header %}
    {% include 'home/page_title_snippet.html'  with page_title='How it Works' %}
{% endblock %}

{% block content%}

<div class="container">
    <div id="how" class="row m-y-2">
        <div class="col-xs-12 text-xs-center m-b-2 cb-section-title ">
            <h1 class="display-4">Getting Started</h1>
        </div>
        <div class="m-y-1 col-xs-6  text-xs-center">
            <span style="font-size:5rem;">1 </span><i class="fa fa-user-plus fa-5x m-y-2" aria-hidden="true"></i>
            <h1>Register your own Raise PH account.</h1>
            <p>Make sure that you register with your legal name to avoid problems in the future.</p>
        </div>
        <div class="m-y-1 col-xs-6  text-xs-center">
            <span style="font-size:5rem;">2 </span><i class="fa fa-check fa-5x m-y-2" aria-hidden="true"></i>
            <h1>Verify your account.</h1>
            <p>Fill up, sign, and upload your Kapandesal Membership Application Form. Wait for your Kapandesal membership status to be 'Waiting for First Deposit'. If you're an existing Kapandesal coop member, just provide us your membership ID number.</p>
        </div>
        <div class="m-y-1 col-xs-6  text-xs-center">
            <span style="font-size:5rem;">3 </span><i class="fa fa-money fa-5x m-y-2" aria-hidden="true"></i>
            <h1>Add funds to your wallet.</h1>
            <p>Once verified, you can now add funds to your digital wallet by depositing it to Kapandesal’s bank account. A one-time membership fee of 2,000 pesos is deducted on your first deposit for new members.</p>
        </div>
        <div class="m-y-1 col-xs-6  text-xs-center">
            <span style="font-size:5rem;">4 </span><i class="fa fa-building fa-5x m-y-2" aria-hidden="true"></i>
            <h1>Choose a project to support</h1>
            <p>Choose a project that suits your risk appetite, and social advocacy. Minimum amount is 5,000 pesos</p>
        </div>
        <div class="m-y-1 col-xs-6  text-xs-center">
            <span style="font-size:5rem;">5 </span><i class="fa fa-line-chart fa-5x m-y-2" aria-hidden="true"></i>
            <h1>Gain returns.</h1>
            <p>Sit back, relax, and watch your money grow. Dividends/income will reflect in your digital wallet.</p>
        </div>
        <div class="m-y-1 col-xs-6  text-xs-center">
            <span style="font-size:5rem;">6 </span><i class="fa fa-repeat fa-5x m-y-2" aria-hidden="true"></i>
            <h1>Re-invest your profits.</h1>
            <p>Compound your returns by re-investing your profits from an existing project to another project.</p>
        </div>
    </div>
    <div id="faqs" class="row m-y-3">
        <div class="col-xs-12 text-xs-center m-b-2 cb-section-title">
            <h1 class="display-4">Frequently Asked Questions</h1>
        </div>

        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>1. How much is the minimum/maximum investment per project?</strong></h5>
            <p class="text-justify">A minimum of 5k can be shelled-in a project. The maximum investment for each Kapandesal Coop members is set to 10% of the total project funding goal. This is done to allow more people to give opportunity to more members.
            </p>
        </div>

        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>2. How does Kapandesal Multipurpose Cooperative choose its investment projects?</strong></h5>
            <p class="text-justify">Kapandesal chooses its projects by checking on the projects': </br>
                1. benefit to the society; </br>
                2. profitability for the Raise PH/Kapandesal Coop members.
            </p>
        </div>

        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>3. What is my expected annual return?</strong></h5>
            <p class="text-justify">TThe expected annual return will depend on each project. A financial prospectus of each project will be provided for Kapandesal Coop members to understand the project’s feasibility. However, the returns are NOT guaranteed, unless stated otherwise.
            </p>
        </div>

        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>4. What are the risks involved?</strong></h5>
            <p class="text-justify"> In general, the risks would include: disaster risks, competitor risks, etc.
                Each project carry its own risks, as written in the investment prospectus.
            </p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>5. How to join?</strong></h5>
            <p class="text-justify">To join, you should sign up as a Kapandesal Coop member by registering an account and filling up the Kapandesal Membership Application form found in the ‘Verify Account’ screen of your account. Note that a one-time membership fee of 2,000 will be deducted from your first deposit to become a Kapandesal Multipurpose Cooperative member.
            </p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>6. What is the minimum and maximum amount for share capital investment?</strong></h5>
            <p class="text-justify">The minimum amount is 5,000 pesos.</p>
            <p class="text-justify">The maximum amount is 10% of the total project cost.</p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>7. How can I deposit funds to my wallet?</strong></h5>
            <p class="text-justify">IIn your Account page, click on ‘Add Funds to My Wallet’ and follow the steps described. All funds will be deposited to a designated bank account owned by Kapandesal Multipurpose Cooperative
            </p>
        </div>
         <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>8. How do I support a project?</strong></h5>
            <p class="text-justify">Add funds to your wallet, Enter your investment amount, password, and click the Invest Now button.</p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>9. What if I want to invest in a project that is already fully funded?</strong></h5>
            <p class="text-justify">You can no longer invest in a project that is already fully funded. You can choose other projects instead.</p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>10. What if I want to refund my share capital investment?</strong></h5>
            <p class="text-justify">Only share capital before a project is fully-funded can be refunded.</p>
            <p class="text-justify">You may request assistance through our <a href="{% url 'contact' %}"><u>Contact Us </u></a> page. </p>
            <p class="text-justify">Note that a service fee of 50 pesos will be deducted from your wallet for each refund transaction. </p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>11. What if I want to invest in a project that is already fully funded?</strong></h5>
            <p class="text-justify">You can no longer invest in a project that is already fully funded. You can choose other projects instead.</p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>12. What happens if a project is not fully funded?</strong></h5>
            <p class="text-justify">Kapandesal Multipurpose Cooperative will exert its best effort to look for partners who will cover the remaining amount. However, if there is none, your share capital will be refunded back to your wallet.</p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>13. When can I receive my dividends?</strong></h5>
            <p class="text-justify">TThe disbursement of the dividends will depend on the project. Refer to the financial prospectus of the project you are interested in.
            </p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>14. How can I assure the safety of my capital?</strong></h5>
            <p class="text-justify">All funds of the members is stored in Kapandesal Multipurpose Cooperative’s bank account/s. The Chief Financial Officer will also monitor all transactions related to the projects. Legal documents are also signed by Kapandesal Multipurpose Cooperative and its partners (if any) to ensure the liabilities of the parties.
            </p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>15. How can I withdraw funds from my wallet?</strong></h5>
            <p class="text-justify">In your Account page, click ‘Withdraw Funds’ and follow the steps described.
            </p>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <h5><strong>16. Who owns the projects that the members are funding?</strong></h5>
            <p class="text-justify">The ownership of the projects will be under the legal name of Kapandesal Multipurpose Cooperative. The members who invested in the projects own share capital that signifies their right to receive dividends/income from the projects.
            </p>
        </div>
    </div>
</div>
{% endblock%}