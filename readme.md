//Install Python dependencies (Django,)
>>pip install -r requirements.txt

>>install npm from nodejs website using installer

//install javascript/npm dependencies (Bower,Grunt) from package.json
>>npm install

//install bower dependencies (Bootstrap, Jquery)
>>bower install


//compile sass .scss files to css
>>grunt

//automatically compile whenever scss changes
>>grunt watch

